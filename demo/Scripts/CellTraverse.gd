extends RayCast

var last_cell;

export (NodePath) var text_node;

const CellGhost = preload("res://Scenes/Prefabs/CellGhost.tscn")
var ghost : MeshInstance;

export (NodePath) var root_cell;

func _init():
	ghost = CellGhost.instance();
	ghost.scale = Vector3.ZERO;

func _ready():
	update_text_and_ghost();

func _enter_tree():
	get_viewport().call_deferred("add_child", ghost);

func _exit_tree():
	ghost.get_parent().remove_child(ghost);

func free():
	ghost.call_deferred("queue_free");

func _physics_process(delta):
	if(is_colliding()):
		if get_collider().has_method("draw_cell"):
			var new_cell = get_collider();
			if last_cell and (new_cell == last_cell):
				return;
			last_cell = new_cell;
			update_text_and_ghost();
		else:
			if last_cell:
				last_cell = null;
				update_text_and_ghost()
			return
	else:
		if last_cell:
			last_cell = null;
			update_text_and_ghost();

func update_text_and_ghost():
	if not last_cell:
		if text_node and get_node(text_node):
			get_node(text_node).set_text("");
		ghost.scale = Vector3.ZERO;
		return;
	
	ghost.scale = last_cell.scale;
	ghost.translation = last_cell.translation;
	
	if text_node and get_node(text_node):
		var point_text = "{material: %d, density: %f}"
		
		var text = """Cell Information
Depth: %d
Child index: %d
Has children: %s

Corner Information
1: %s
2: %s
3: %s
4: %s
5: %s
6: %s
7: %s
8: %s"""
		
		var format = [
			last_cell.cell.get_depth(), last_cell.child_index,
			("yes" if last_cell.cell.has_children() else "no")
		]
		
		for i in range(0,8):
			var corner = last_cell.cell.get_corner(i);
			format.append(point_text % [corner.material, corner.density]);
		
		get_node(text_node).set_text(text % format);

func _unhandled_input(event):
	if InputMap.event_is_action(event, "subdivide_target") and event.is_pressed():
		if last_cell:
			last_cell.subdivide();
			if root_cell and get_node(root_cell) and get_node(root_cell).has_method("draw_tree"):
				get_node(root_cell).call_deferred("draw_tree");
	elif InputMap.event_is_action(event, "undivide_target") and event.is_pressed():
		if last_cell:
			last_cell.collapse();
			if root_cell and get_node(root_cell) and get_node(root_cell).has_method("draw_tree"):
				get_node(root_cell).call_deferred("draw_tree");