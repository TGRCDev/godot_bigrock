extends CollisionObject

var cell : Reference;
var child_index = 0;

var cellref_children = [];

const CELL_DRAW_VERTS = [
	Vector3(0,0,0),
	Vector3(1,0,0),
	Vector3(1,0,0),
	Vector3(1,1,0),
	Vector3(1,1,0),
	Vector3(0,1,0),
	Vector3(0,1,0),
	Vector3(0,0,0),
	Vector3(0,0,1),
	Vector3(1,0,1),
	Vector3(1,0,1),
	Vector3(1,1,1),
	Vector3(1,1,1),
	Vector3(0,1,1),
	Vector3(0,1,1),
	Vector3(0,0,1),
	Vector3(0,0,0),
	Vector3(0,0,1),
	Vector3(1,0,0),
	Vector3(1,0,1),
	Vector3(1,1,0),
	Vector3(1,1,1),
	Vector3(0,1,0),
	Vector3(0,1,1)
]

const CELL_VERTS = [
	Vector3(0,0,0),
	Vector3(1,0,0),
	Vector3(0,1,0),
	Vector3(1,1,0),
	Vector3(0,0,1),
	Vector3(1,0,1),
	Vector3(0,1,1),
	Vector3(1,1,1)
]

func get_child_pos(index : int):
	if not cell:
		return Vector3(0,0,0);
	
	var subdiv = 1.0 / pow(2, cell.get_depth());
	return CELL_VERTS[index] * (subdiv / 2);

func get_relative_scale():
	assert(cell);
	if not cell:
		return 1;
	
	return 1.0 / pow(2, cell.get_depth());

func draw_cell(ig : ImmediateGeometry):
	if not cell:
		return;
	
	var subdiv = 1.0 / pow(2, cell.get_depth());
	
	for point in CELL_DRAW_VERTS:
		ig.add_vertex(self.translation + (point * subdiv))
	
	for child in get_children():
		if child.has_method("draw_cell"):
			child.draw_cell(ig);

func subdivide():
	if not cell or cell.has_children():
		return;
	
	cell.subdivide();
	cellref_children.resize(8);
	for i in range(0, 8):
		var node = Area.new();
		node.set_script(self.get_script());
		node.add_child($CollisionShape.duplicate());
		node.cell = cell.get_child(i);
		node.child_index = i;
		node.translation = self.translation + get_child_pos(i);
		node.scale = Vector3.ONE * (get_relative_scale() / 2);
		add_child(node);
		cellref_children[i] = node;
	$CollisionShape.disabled = true;

func collapse():
	get_parent().call_deferred("undivide");

func undivide():
	if not cell or cell.is_leaf():
		return;
	
	for child in cellref_children:
		if child.has_method("undivide"):
			child.undivide();
		child.queue_free();
	
	cellref_children.clear();
	cell.undivide();
	$CollisionShape.disabled = false;
