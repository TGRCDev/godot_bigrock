extends Spatial

enum Camera_Mode {
	Free, # Player can move and rotate the camera freely
	Rotate, # Player can rotate the camera, but the camera's position locked to the target's origin
	Scripted # Player cannot rotate or move the camera
}

export(Camera_Mode) var camera_mode = Camera_Mode.Free

# Speed in units per second
export (float) var speed = 3.0;
export (float, 0.1, 2.0) var sensitivity = 0.7;
export (bool) var interpolated = true;
export (float, 0.1, 1.0) var interpolate_speed = 0.25;

export (bool) var clamp_x_look = false;
export (float, -179, 179) var clamp_x_min_value = -89;
export (float, -179, 179) var clamp_x_max_value = 89;
export (bool) var clamp_y_look = true;
export (float, -179, 179) var clamp_y_min_value = -89;
export (float, -179, 179) var clamp_y_max_value = 89;

var _forward : bool = false;
var _backward : bool = false;
var _left : bool = false;
var _right : bool = false;
var _up : bool = false;
var _down : bool = false;

onready var target_origin = self.translation;
onready var _current_degrees = self.rotation_degrees; # Gotta store this to properly interpolate
onready var target_degrees = self.rotation_degrees;

func _process(delta):
	if camera_mode == Camera_Mode.Free:
		var movement = Vector3()
		if _forward and not _backward:
			movement.z -= 1;
		elif _backward and not _forward:
			movement.z += 1;
		
		if _left and not _right:
			movement.x -= 1;
		elif _right and not _left:
			movement.x += 1;
		
		if _up and not _down:
			movement.y += 1;
		if _down and not _up:
			movement.y -= 1;
		
		target_origin += self.transform.basis.xform(movement).normalized() * delta * speed;
	
	# modulo on degree values, prevent overflow
	if interpolated:
		if (_current_degrees.x <= -360 and target_degrees.x < -360) or (_current_degrees.x >= 360 and target_degrees.x >= 360):
			_current_degrees.x = fmod(_current_degrees.x, 360)
			target_degrees.x = fmod(target_degrees.x, 360)
		if (_current_degrees.y <= -360 and target_degrees.y <= -360) or (_current_degrees.y >= 360 and target_degrees.y >= 360):
			_current_degrees.y = fmod(_current_degrees.y, 360)
			target_degrees.y = fmod(target_degrees.y, 360)
	else: # Non-interpolated movement still needs modulo to prevent eventual precision loss from extremely large values
		_current_degrees.x = fmod(_current_degrees.x, 360)
		_current_degrees.y = fmod(_current_degrees.y, 360)
		target_degrees.x = fmod(target_degrees.x, 360)
		target_degrees.y = fmod(target_degrees.y, 360)
	
	if clamp_x_look:
		target_degrees.y = clamp(target_degrees.y, clamp_x_min_value, clamp_x_max_value);
	if clamp_y_look:
		target_degrees.x = clamp(target_degrees.x, clamp_y_min_value, clamp_y_max_value)
	
	if interpolated:
		_current_degrees = lerp(_current_degrees, target_degrees, interpolate_speed);
	else:
		_current_degrees = target_degrees;
	
	if interpolated:
		self.translation = lerp(self.translation, target_origin, interpolate_speed);
	else:
		self.translation = target_origin;
	
	self.rotation_degrees = _current_degrees;

func _unhandled_input(event):
	if event is InputEventMouseMotion and camera_mode != Camera_Mode.Scripted and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		target_degrees -= Vector3(event.relative.y, event.relative.x, 0) * sensitivity;
	else:
		if InputMap.event_is_action(event, "move_forward"):
			_forward = event.pressed;
		elif InputMap.event_is_action(event, "move_backward"):
			_backward = event.pressed;
		elif InputMap.event_is_action(event, "move_left"):
			_left = event.pressed;
		elif InputMap.event_is_action(event, "move_right"):
			_right = event.pressed;
		elif InputMap.event_is_action(event, "jump"):
			_up = event.pressed;
		elif InputMap.event_is_action(event, "crouch"):
			_down = event.pressed;
		elif InputMap.event_is_action(event, "toggle_mouse") and event.pressed:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE else Input.MOUSE_MODE_VISIBLE);
