#include "point.hpp"

namespace godot
{
    float Point::get_density() const
    {
        return data->density;
    }

    void Point::set_density(float value)
    {
        data->density = value;
    }

    unsigned char Point::get_material() const
    {
        return data->material;
    }

    void Point::set_material(unsigned char value)
    {
        data->material = value;
    }

    void Point::_register_methods()
    {
        register_property<Point, float>("density", &Point::set_density, &Point::get_density, -1.0f);
        register_property<Point, unsigned char>("material", &Point::set_material, &Point::get_material, 0);
        register_method("_init", &Point::_init);
    }
}