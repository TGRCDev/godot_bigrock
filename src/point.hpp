#ifndef GODOT_BIGROCK_POINT_HPP
#define GODOT_BIGROCK_POINT_HPP
#pragma once

#include <Godot.hpp>
#include <Reference.hpp>

#include <memory>

#include <data/point.hpp>

namespace godot {

    // Wrapper around bigrock::Point
    class Point : public Reference
    {
        GODOT_CLASS(Point, Reference);

        std::shared_ptr<bigrock::Point> data;

        void set_data(bigrock::Point *new_data) {this->data.reset(new_data);}

        public:
        Point() : data(std::shared_ptr<bigrock::Point>(new bigrock::Point())) {};

        static void _register_methods();

        void _init() {};

        float get_density() const;
        void set_density(float value);

        unsigned char get_material() const;
        void set_material(unsigned char value);

        friend class Cell;
    };

}



#endif