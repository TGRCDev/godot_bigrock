#include "cell.hpp"
#include "point.hpp"

namespace godot {

    void Cell::_register_methods()
    {
        register_method("_init", &Cell::_init);
        register_method("get_corner", &Cell::get_corner);
        register_method("get_child", &Cell::get_child);
        register_method("has_children", &Cell::has_children);
        register_method("is_leaf", &Cell::is_leaf);
        register_method("subdivide", &Cell::subdivide);
        register_method("undivide", &Cell::undivide);
        register_method("get_depth", &Cell::get_depth);
    }

    Ref<Point> Cell::get_corner(unsigned char index)
    {
        ERR_FAIL_COND_V(index > 7, Ref<Point>());
        ERR_FAIL_COND_V(!data, Ref<Point>());

        Ref<Point> ret;
        ret.instance();
        (*ret)->set_data(&data->get_corner(index));
        return ret;
    }

    Ref<Cell> Cell::get_child(unsigned char index)
    {
        ERR_FAIL_COND_V(index > 7, Ref<Cell>());
        ERR_FAIL_COND_V(!data, Ref<Cell>());
        ERR_FAIL_COND_V(is_leaf(), Ref<Cell>());

        Ref<Cell> ret;
        ret.instance();
        (*ret)->set_data(data->get_child(index), false);
        return ret;
    }

    unsigned char Cell::get_depth() const
    {
        ERR_FAIL_COND_V(!data, 0);
        return data->get_depth();
    }

    void Cell::subdivide()
    {
        ERR_FAIL_COND(!data);

        if(has_children())
            return;
        
        data->subdivide();
    }

    void Cell::undivide()
    {
        ERR_FAIL_COND(!data);

        if(is_leaf())
            return;
        
        data->undivide();
    }
}