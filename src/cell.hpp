#ifndef GODOT_BIGROCK_CELL_H
#define GODOT_BIGROCK_CELL_H
#pragma once

#include <Godot.hpp>
#include <Reference.hpp>
#include <Ref.hpp>

#include <data/cell.hpp>

namespace godot
{
    class Point;

    class Cell : public Reference
    {
        GODOT_CLASS(Cell, Reference);

        bigrock::Cell *data;
        bool owns_data;

        void set_data(bigrock::Cell *new_data, bool owned) {
            if(data && owns_data)
                delete data;
            
            data = new_data;
            owns_data = owned;
        }

        public:
        Cell() : data(new bigrock::Cell()), owns_data(true) {};
        

        ~Cell()
        {
            if(owns_data)
                delete data;
        }

        static void _register_methods();

        void _init() {};

        bool has_children() const {

            return data->has_children();
        }
        bool is_leaf() const {return data->is_leaf();}

        Ref<godot::Point> get_corner(unsigned char index);
        //const Point get_corner(unsigned char index) const;

        Ref<godot::Cell> get_child(unsigned char index);
        //const Cell get_child(unsigned char index) const;

        unsigned char get_depth() const;

        void subdivide();
        void undivide();
    };
}

#endif